<?php
require_once (__DIR__ . '/../controller/IndexController.php');

$cnt = new IndexController();
$authors = $cnt->getInfoAuthors();
?>
<html>
<head>
<title>Quotes List</title>
</head>
<body>
	<div id="wrapper">
		<h1>Quotes List</h1>
		<table>
			<tr>
				<th>Author</th>
				<th>Quote</th>
			</tr>
			<?php foreach($authors as $author){
			    $quotes = $author->getQuotes();
			    foreach($quotes as $q){?>
		           <!-- Filas de la tabla  -->
                <tr>
                    <td><?=$author->getName()?></td>
                    <td><?=$q?></td>
                </tr>

			<?php }} ?>
		</table>
	</div>
</body>
</html>