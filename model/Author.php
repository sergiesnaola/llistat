<?php

class Author
{

    private $_name;

    private $_quotes;

    public function __construct($n)
    {
        $this->_name = $n;
        $this->_quotes = array();
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setQuote($q)
    {
        array_push($this->_quotes, $q);
    }

    public function getQuotes()
    {
        return $this->_quotes;
    }
}