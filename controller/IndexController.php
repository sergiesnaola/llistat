<?php
require_once (__DIR__ . '/../model/Author.php');

class IndexController{
    private $authors = ['Plato', 'Aristotle', 'Herodotus', 'Socrates'];
    private $quotes = [
    'Plato' => [
        "Wise men speak because they have something to say; Fools because they have to say something.",
        "Music is the movement of sound to reach the soul for the education of its virtue.",
        "The beginning is the most important part of the work."    
    ],
    
    'Aristotle' => [
        "Quality is not an act, it is a habit.",
        "Pleasure in the job puts perfection in the work.",
        "The roots of education are bitter, but the fruit is sweet."
    ],
    
    'Herodotus' => [
        "Of all possessions a friend is the most precious.",
        "In peace, sons bury their fathers. In war, fathers bury their sons.",
        "Men trust their ears less than their eyes."
    ],
    
    'Socrates' => [
        "The only true wisdom is in knowing you know nothing.",
        "Death may be the greatest of all human blessings.",
        "True knowledge exists in knowing that you know nothing."
    ]
];
    
    public function getInfoAuthors(){
        $ret = array();
        foreach($this->authors as $author){
            $reta = new Author($author); 
            foreach($this->quotes[$author] as $quote){ 
                // A�adir Frases al Autor
                $reta->setQuote($quote);
            }
            array_push($ret, $reta);
            
            // A�adir autor a la lista
        }
        return $ret;
        
        // Devolver return
    }
    
}